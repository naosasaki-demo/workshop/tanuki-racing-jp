# イントロダクション

このチャレンジでは、既存のパイプラインを変更して、GitLabの[`Rules`](https://docs.gitlab.com/ee/ci/yaml/#rules)と[`allow_failure`](https://docs.gitlab.com/ee/ci/yaml/#allow_failure)キーワードの有用性を示します。

# ステップ01 - ルールの追加

1. まず、前のトラックの既存のパイプラインをリセットし、一時的なDAGジョブをすべて削除する必要があります。**_workshop-project_**プロジェクトにいることを確認し、左側のナビゲーションメニューを使用して**Build -\> Pipeline editor**をクリックして、パイプラインの.ymlファイルを編集します。
2. 以下のコードブロックと一致するコードを削除し、他のすべてのコードはそのままにしてください:

   ```plaintext

    build_car_a:
      stage: build
      script:
        - echo "build_car_a"

    build_car_b:
      stage: build
      script:
        - echo "build_car_b"

    build_car_c:
      stage: build
      script:
        - echo "build_car_c"

    build_car_d:
      stage: build
      script:
        - echo "build_car_d"

    build_car_e:
      stage: build
      script:
        - echo "build_car_e"

    build_car_f:
      stage: build
      script:
        - echo "build_car_f"

    test_car_a:
      stage: test
      needs: [build_car_a]
      script:
        - echo "test_car_a"

    test_car_b:
      stage: test
      needs: [build_car_b]
      script:
        - echo "test_car_b"

    test_car_c:
      stage: test
      needs: [build_car_c]
      script:
        - echo "test_car_c"

    test_car_d:
      stage: test
      needs: [build_car_d]
      script:
        - echo "test_car_d"

    test_car_e:
      stage: test
      needs: [build_car_e]
      script:
        - echo "test_car_e"

    test_car_f:
      stage: test
      needs: [build_car_f]
      script:
        - echo "test_car_f"

    race_car_a:
      stage: race
      needs: [test_car_a]
      script:
        - echo "race_car_a"

    race_car_b:
      stage: race
      needs: [test_car_b]
      script:
        - echo "race_car_b"

    race_car_c:
      stage: race
      needs: [test_car_c]
      script:
        - echo "race_car_c"

    race_car_d:
      stage: race
      needs: [test_car_d]
      script:
        - echo "race_car_d"

    race_car_e:
      stage: race
      needs: [test_car_e]
      script:
        - echo "race_car_e"

    race_car_f:
      stage: race
      needs: [test_car_f]
      script:
        - echo "race_car_f"
   ```

   and fix the stages:

   ```plaintext
   stages:
     - build
     - test
   ```
3. 私たち全員が同じポイントに戻っていることを確認するために、.gitlab-ci.ymlのコードを以下に示します:

 ```plaintext
image: docker:latest

services:
  - docker:dind

variables:
  CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA
  DOCKER_DRIVER: overlay2
  ROLLOUT_RESOURCE_TYPE: deployment
  DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
  RUNNER_GENERATE_ARTIFACTS_METADATA: "true"
  

stages:
  - build
  - test
 
build:
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  before_script:
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $IMAGE .
  after_script:
    - docker push $IMAGE

test:
  stage: test
  image: gliderlabs/herokuish:latest
  script:
    - cp -R . /tmp/app
    - /bin/herokuish buildpack test
  after_script:
    - echo "Our race track has been tested!"
  needs: []


super_fast_test:
  stage: test
  script:
   - echo "If youre not first youre last"
   - return 0
  needs: []   

 ```

4. さて、いよいよルールを追加しよう。まずは、**_test_**ジョブに関する基本的なものから始めましょう。このジョブがmainにマージされるときだけ実行されるようにします。そのためには、以下のルール定義を **_test_** ジョブの最後に追加します:

   ```plaintext
   rules:
     - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
   ```
5. Before committing the changes we are going to add **_cache_** and then **_allow_failure_** keyword in the next steps.

# :rocket: Step 02 - Pipeline Speed With Caching

1. We saw after committing our last change that the pipeline does take some time to run. Our next few pipelines will build off of our current configuration, so why dont we add some caching for the _vendor/ruby & Gemfile.lock_ so that they dont have to be pulled next time.
2. Lets go ahead add the code below right under our **_image_** definition so that our node modules will be cached:

```plaintext
# Cache modules in between jobs
cache:
  - key: cache-$CI_COMMIT_REF_SLUG
    fallback_keys:
      - cache-$CI_DEFAULT_BRANCH
      - cache-default
    paths:
      - vendor/ruby
      - Gemfile.lock
```
3. このコードを追加すると、共有ランナーはこの情報をGCP Cloud Storageにキャッシュし、次回パイプラインを実行するときに取り出せるようにする。コミットする前に、次のステップを実行します。

# Step 03 - Adjusting for Failure

1. **_super_fast_test_** ジョブが異常終了するケースを想定しましょう。**existing script _- echo "If youre not first youre last"_** と **_- return 0_** を以下のように変更し、ジョブを意図的に失敗させるようにします:

   ```plaintext
   - exit 1
   ```
2. また、ジョブの失敗を許容するようなルールをセットしたい場合はどうすれば良いでしょうか？ **_super_fast_test_** ジョブを **変更** し、以下のようにrulesブロックを追加します:

   ```plaintext
   rules:
       - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
         allow_failure: true
   ```
3. **_super_fast_test & unit_test_** ジョブのyamlを以下のように確認します:

```plaintext
test:
  stage: test
  image: gliderlabs/herokuish:latest
  script:
    - cp -R . /tmp/app
    - /bin/herokuish buildpack test
  after_script:
    - echo "Our race track has been tested!"
  needs: []
  rules:
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

super_fast_test:
 stage: test
 script:
 - exit 1
 needs: []
 rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      allow_failure: true
```
4. **Commit Changes**をクリックして、左側のメニューを使用して**Build -\> Pipelines**に進み、最も最近開始されたパイプラインの**_#_**で始まるハイパーリンクをクリックします。ジョブが実行されるにつれて、追加した**_rules & allow_failure_**が出力を変更したかどうかを確認するために、それぞれのジョブに入ってみてください。また、super_fast_testジョブが警告とともに失敗するが、パイプラインは成功することも確認できるはずです。後でパイプラインにさらにステージを追加するので、このジョブが失敗してもパイプラインが続行されることがわかります。なお、完了した**_rules & allow_failure_**を使用すると、ルールのターゲットブランチが変更されることに注意してください。

> 問題が発生した場合は、左側のナビゲーションメニューを使用して、**Build -\> Pipelines** をクリックし、**Run pipeline** をクリックし、**_rules-and-failures_** を選択し、再度 **Run pipeline** をクリックしてください。
