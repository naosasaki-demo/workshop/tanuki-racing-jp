# イントロダクション

このチャレンジでは、子パイプラインのセットアップ方法について説明します。

# ステップ 01 - 子パイプラインの準備

1. セキュリティチームは、パイプラインに追加したすべてのセキュリティスキャンについて情報を得ました。彼らは、アクセス権を持ち、実行するジョブを制御できる新しいパイプラインにそれを追加することを望んでいます。これを行うために、既存のパイプラインからリンクできる子パイプラインを作成します。この場合、このプロジェクトに第2のパイプラインを追加するだけですが、別のプロジェクトに追加してリンクすることもできます。
2. このパイプラインを作成するには、まずプロジェクトの名前を左上にクリックし、メインのプロジェクトページから**編集 > Web IDE**をクリックします。IDEに入ったら、プロジェクトのトップレベルで新しいフォルダを作成し、**security-pipeline**と呼びます。
3. **_security-pipeline_**フォルダ内で、**security.gitlab-ci.yml**という名前の新しいファイルを作成します。そのプロジェクトに移動します。
4. 以下のコードを新しいパイプラインの設定にコピーして貼り付けます。既存のワークショップで作業してきたパイプラインからほとんどの部分が引き継がれていることに注意してください。

   ```plaintext
   image: docker:latest
   
   include:
     - template: Code-Quality.gitlab-ci.yml
     - template: Jobs/Dependency-Scanning.gitlab-ci.yml
     - template: Jobs/SAST.gitlab-ci.yml
     - template: Jobs/Secret-Detection.gitlab-ci.yml
   ```
5. このパイプラインには、セキュリティテストを実行するために必要なすべてが含まれています。
6. 追加したら、左側のソースコントロールボタンをクリックし、簡単なコミットメッセージを追加し、**Commit & 'main**をクリックします。完了したら、ポップアップが表示され、**プロジェクトに移動**をクリックします。
7. プロジェクトに戻ったら、左側のナビゲーションメニューを使用して、**Build \> Pipeline editor**をクリックして、セキュリティパイプラインへの接続を設定します。

# ステップ 02 - パイプラインのリンク

1. まず、既存のパイプラインから重複するコードをすべて削除する必要があります。以下のコードは、セキュリティパイプラインがカバーするすべての内容を除いた既存のパイプラインです。

```plaintext
image: docker:latest

cache:
  - key: cache-$CI_COMMIT_REF_SLUG
    fallback_keys:
      - cache-$CI_DEFAULT_BRANCH
      - cache-default
    paths:
      - vendor/ruby

services:
  - docker:dind

variables:
  CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA
  DOCKER_DRIVER: overlay2
  ROLLOUT_RESOURCE_TYPE: deployment
  DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
  RUNNER_GENERATE_ARTIFACTS_METADATA: "true"
  SECRET_DETECTION_EXCLUDED_PATHS: "Courses"

stages:
  - build
  - test
 
build:
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  before_script:
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $IMAGE .
  after_script:
    - docker push $IMAGE
  artifacts:
    paths:
      - Gemfile.lock
    expire_in: 1 hour

test:
 stage: test
 image: gliderlabs/herokuish:latest
 script:
   - cp -R . /tmp/app
   - /bin/herokuish buildpack test
 after_script:
   - echo "Our race track has been tested!"
 needs: []
 rules:
   - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

super_fast_test:
 stage: test
 script:
   - exit 1
 needs: []
 rules:
   - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
     allow_failure: true
```
2. 子パイプラインのための新しいステージを追加しましょう。ステージセクションを以下のように編集します。

   ```plaintext
   stages:
       - build
       - test
       - extra-security
   ```
3. セキュリティステージで子パイプラインを呼び出すジョブも追加しましょう。

```plaintext
   downstream_security:
     stage: extra-security
     trigger:
       include:
             - local: security-pipeline/security.gitlab-ci.yml
   ```
4. さあ、**変更をコミット**をクリックし、左側のメニューから**ビルド \> パイプライン**をクリックし、最新のパイプラインのハイパーリンクをクリックします。ハイパーリンクは**<span dir="">_#_</span>**で始まります。パイプラインビューでは、ジョブが実行されるたびに、追加した**_child-security-pipeline_**が出力にどのように変更されたかを確認するために、それぞれのジョブにクリックしてください。

> 問題が発生した場合は、左側のナビゲーションメニューを使用して**ビルド \> パイプライン**をクリックし、**パイプラインを実行**をクリックし、**_child-security-pipeline_**を選択してから再度**パイプラインを実行**をクリックしてください。

