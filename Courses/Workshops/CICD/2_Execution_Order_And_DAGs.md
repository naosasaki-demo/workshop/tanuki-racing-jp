# イントロダクション

このチャレンジでは、最初のトラックで作成したシンプルなパイプラインをベースにして、実行順序を変更し、DAG（有向非巡回グラフ）を作成する方法を紹介します。

# ステップ 01 - 実行順序

1. もし前のトラックから直接このページに来ている場合は、まだパイプラインページにいるはずです。もし他のページに移動してしまった場合は、プロジェクトに戻り、左側のナビゲーションメニューから **Build -\> Pipelines** をクリックし、最後に実行されたパイプラインの **_#_** で始まるハイパーリンクをクリックしてください。

> 例: **_#577244133_**

2. パイプラインのジョブは順番に実行されていることがわかりますが、もし2つのジョブを並行して実行したい場合はどうすればよいでしょうか？それには **_needs_** キーワードを使用することができます。これを実現するために、**_.gitlab-ci.yml_** ファイルを編集しましょう。
3. パイプラインを編集するには、左側のナビゲーションメニューから **Build -\> Pipeline editor** をクリックしてください。現在はテストステージで **_test_** ジョブのみが実行されていますので、**_test_** ジョブの下に **_super_fast_test_** ジョブを追加しましょう。

   ```plaintext
   super_fast_test:
     stage: test
     script:
       - echo "If youre not first youre last"
       - return 0
     needs: []
   ```
4. 2つのジョブが追加されたので、実行順序も変更して同時に実行されるようにしたいと思います。以下の行を **_test_** コードブロックの最後に追加してください。

   ```plaintext
   needs: []
   ```

   新しい **_test_** は以下のようになります:

   ```plaintext
   test:
     stage: test
     image: gliderlabs/herokuish:latest
     script:
       - cp -R . /tmp/app
       - /bin/herokuish buildpack test
     after_script:
       - echo "Our race track has been tested!"
     needs: []
   ```

5. **Commit changes** をクリックして変更を保存し、左側のナビゲーションメニューから **Build -\> Pipelines** をクリックし、最後に実行されたパイプラインの **_#_** で始まるハイパーリンクをクリックしてください。実行を見ていると、2つのジョブが並行して実行されることがわかります。

> もし問題が発生した場合は、左側のナビゲーションメニューから **Build -\> Pipelines** をクリックし、**Run pipeline** をクリックし、**_execution-order_** を選択して再度 **Run pipeline** をクリックしてください。

# ステップ 02 - DAG（有向非巡回グラフ）

1. では、もし多くのステージとジョブ間の関係を持つパイプラインをできるだけ早く実行したい場合はどうすればよいでしょうか？それには DAG を使用することができます。左側のナビゲーションメニューから **Build -\> Pipeline editor** をクリックして、DAG を作成しましょう。
2. このワークショップでは完全な DAG を作成することはしませんが、代わりに DAG レースを作成します。まず、ステージセクションの下に **_race_** ステージを追加する必要がありますので、コードは以下のようになります:

```plaintext
   stages:
     - build
     - test
     - race
   ```
3. 下記は、**_super_fast_test_** ジョブの下に追加したいジョブのリストです。数が多いため、コードをコピーして貼り付ける方が良いでしょう。

   ```plaintext
   build_car_a:
    stage: build
    script:
     - echo "build_car_a"

   build_car_b:
    stage: build
    script:
     - echo "build_car_b"

   build_car_c:
    stage: build
    script:
     - echo "build_car_c"

   build_car_d:
    stage: build
    script:
     - echo "build_car_d"

   build_car_e:
    stage: build
    script:
     - echo "build_car_e"

   build_car_f:
    stage: build
    script:
     - echo "build_car_f"

   test_car_a:
    stage: test
    needs: [build_car_a]
    script:
     - echo "test_car_a"

   test_car_b:
    stage: test
    needs: [build_car_b]
    script:
     - echo "test_car_b"

   test_car_c:
    stage: test
    needs: [build_car_c]
    script:
     - echo "test_car_c"

   test_car_d:
    stage: test
    needs: [build_car_d]
    script:
     - echo "test_car_d"

   test_car_e:
    stage: test
    needs: [build_car_e]
    script:
     - echo "test_car_e"

   test_car_f:
    stage: test
    needs: [build_car_f]
    script:
     - echo "test_car_f"

   race_car_a:
    stage: race
    needs: [test_car_a]
    script:
     - echo "race_car_a"

   race_car_b:
    stage: race
    needs: [test_car_b]
    script:
     - echo "race_car_b"

   race_car_c:
    stage: race
    needs: [test_car_c]
    script:
     - echo "race_car_c"

   race_car_d:
    stage: race
    needs: [test_car_d]
    script:
     - echo "race_car_d"

   race_car_e:
    stage: race
    needs: [test_car_e]
    script:
     - echo "race_car_e"

   race_car_f:
    stage: race
    needs: [test_car_f]
    script:
     - echo "race_car_f"
   ```
4. もしも「Visualize」タブをクリックすると、多くのステージがどれだけ複雑かがわかります。では、**_Edit_** タブに戻り、「Commit changes」をクリックしましょう。
5. コミットが完了したら、左側のナビゲーションメニューから **Build -\> Pipelines** をクリックし、最新のパイプラインの開始時に始まる **_#_** で始まるハイパーリンクをクリックします。ここで、すべての車が最初に完了するパスを見ることができます。

> 例: **_#577244133_**

> もし問題が発生した場合は、左側のナビゲーションメニューから **Build -\> Pipelines** をクリックし、**Run pipeline** をクリックし、**_DAG_** を選択してから再度 **Run pipeline** をクリックしてください。
