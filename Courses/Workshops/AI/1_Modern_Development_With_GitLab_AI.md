# GitLab Duo ワークショップへようこそ :fox:

[[_TOC_]]

## :dart: イントロダクション

このワークショップの目的は、GitLab AIチームが開発しているすべての機能を紹介することです。コードの提案だけでなく、ソフトウェアのデリバリーライフサイクル（SDLC）全体でのサポートに重点を置いています。

**注意:** これらの機能の一部は[実験フェーズ](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#experiment)にあり、開発チームが積極的に改善を行っているため、障害が発生する可能性があります。各機能の成熟度レベルについては、[こちら](https://docs.gitlab.com/ee/user/ai_features.html)のドキュメントをご覧ください。

## :checkered_flag: スタート

### 1\. GitLab Duo Chatを開く

このワークショップでは、GitLab Duo ChatからAIによるサポートを受けることができます。

- [ ] チャットを開くには以下の手順を実行してください:
  1. 左下の角にある`ヘルプ`アイコンを選択します。
  2. `GitLab Duo Chat`を選択します。右側にドロワーが開きます。
  3. 予め定義された質問のいずれかをクリックします（または自分自身の質問をすることもできます）。
  4. `/reset`と入力してコンテキストをクリアします。
  5. 最後に、`/clean`メッセージで以前の会話をすべて削除します。

### 2\. コードの提案を有効にする

 - すべてのAI機能は、既にトップグループレベルで[有効になっています](https://docs.gitlab.com/ee/user/ai_features.html#enable-aiml-features)。
    - コードの提案は、アカウントが所属するトップレベルグループの少なくとも1つで有効にする必要があります。この環境ではすでに対応済みです。自分の環境でこの機能を実装する際には、トップレベルグループの設定を表示できる役割がない場合は、グループのオーナーに連絡してください。
    - 参考:
      - [GitLab SaaSでのコードの提案](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/saas.html)
      - [自己管理型GitLabでのコードの提案の有効化](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/self_managed.html#enable-code-suggestions-on-self-managed-gitlab)

 #### 代替オプション

Ask GitLab Duo Chat :robot: に質問して、指示に従ってください。

```plaintext
自分のアカウントでコードの提案を有効にする方法は？
```

## :memo: 作業計画を立てる

### 1\. イシューを作成する

プロジェクトマネージャーから、GitLabのパイプラインをセキュアにし、見つかった脆弱性を修正するタスクが割り当てられました。作業を追跡するために、GitLabのプラン機能を使用します。

- [ ] 最初のイシューを作成するには：

1. 左側のナビゲーションメニューから **Plan > issues** をクリックします。
2. 次に **New Issue** をクリックします。
3. イシューに簡単な名前を付け、説明セクションの **tanukiアイコン** をクリックします。
4. ドロップダウンで **Generate issue description** をクリックし、ポップアップで "Secure our CICD pipeline and fix the newfound vulnerabilities" と入力します。
5. 次に **submit** をクリックし、説明が自動的に入力されることに気づきます。
6. イシューを自分自身に割り当て、期限を設定し、**create issue** をクリックします。

## :checkered_flag: パイプラインの実行

### 1\. プロジェクトのデータを生成する

ワークショップの残りの部分で使用するために、プロジェクトのパイプラインを実行してプロジェクトに有用な情報を生成する必要があります（脆弱性レポートなど）。

- [ ] パイプラインを手動で実行するには：

1. 左側のナビゲーションメニューから **Build \> Pipeline editor** をクリックし、現在のパイプライン設定を以下のコードで置き換えます：

    ```
    image: docker:latest

    services:
      - docker:dind

    variables:
      CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA
      DOCKER_DRIVER: overlay2
      ROLLOUT_RESOURCE_TYPE: deployment
      DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
      RUNNER_GENERATE_ARTIFACTS_METADATA: "true"
      CI_DEBUG_TRACE: "true"
      SECRET_DETECTION_EXCLUDED_PATHS: "Courses"
      

    stages:
      - build
      - test

    include:
      - template: Jobs/Test.gitlab-ci.yml
      - template: Jobs/Container-Scanning.gitlab-ci.yml
      - template: Code-Quality.gitlab-ci.yml
      - template: Jobs/Dependency-Scanning.gitlab-ci.yml
      - template: Jobs/SAST.gitlab-ci.yml
      - template: Jobs/Secret-Detection.gitlab-ci.yml
      - template: Jobs/SAST-IaC.gitlab-ci.yml

    build:
      stage: build
      variables:
        IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
      before_script: 
        - apt list --installed   
      script:
        - docker info
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - docker build -t $IMAGE .
        - docker push $IMAGE

    sast:
      variables:
          SEARCH_MAX_DEPTH: 12

    xray:
      stage: build
      image: registry.gitlab.com/gitlab-org/code-creation/repository-x-ray:latest
      allow_failure: true
      rules:
      - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        OUTPUT_DIR: reports
      script:
        - x-ray-scan -p "$CI_PROJECT_DIR" -o "$OUTPUT_DIR"
      artifacts:
        reports:
          repository_xray: "$OUTPUT_DIR/*/*.json"
    ```

  > また、**_ai-main-branch_** ブランチを main にマージすることもできます。

2. 変更したら、スクロールして **Commit changes** をクリックします。その後、このパイプラインを実行し、ワークショップの後で戻ってきます。

> パイプラインが実行されており、ワークショップの後で戻ってきます。その間、GitLab Duo Chatを使った他のユースケースをテストしてみましょう。

### 2\. 壊れたパイプラインのトラブルシューティング

- [ ] 壊れたパイプラインを修正するには：

1. キックオフしたパイプラインがビルドジョブに失敗したことに気づきます。この問題を修正するために、新しいRoot Cause Analysis機能を使用します。このツールを使用すると、失敗したパイプラインのトラブルシューティングが簡単になります。
2. 左側のナビゲーションメニューから **Build \> Pipelines** をクリックし、最後にキックオフされたパイプラインに移動します。
3. _build_ ジョブが失敗することを予想し、失敗したらジョブに移動します。ビューの上部で **Root cause analysis** をクリックし、画面の左側に詳細な分析が表示されます。この場合、ビルドジョブに追加した `apt list --installed` の行がイメージでサポートされていないためです。
4. これを修正するには、左側のナビゲーションメニューから **Build > Pipeline Editor** をクリックし、before_scriptコードがある33-34行のコードを削除します。
5. 最後に、スクロールして **Commit changes** をクリックして、正常に動作するパイプラインをキックオフします。

### 3. GitLab Duo Chatにもっと頼む

- [ ] GitLab Duo Chat :robot: に、パイプラインの設定ファイルを作成するように依頼する。

  ```plaintext
  Ruby Sinatraアプリケーションをテストおよびビルドするための.gitlab-ci.yml設定ファイルを作成する
  ```
- [ ] GitLab Duo Chat :robot: に、コードのドキュメントを作成するように依頼する。
  1. 左のサイドバーで、`Code > Repository`を選択します。
  2. lib/tanukiracing/app.rbをクリックします。
  3. 最初にプロジェクト名を左上にクリックし、次にtanuki-racing/lib/tanukiracing/app.rbをクリックします。
  4. GitLab Duo Chat :robot: に依頼します。

    ```plaintext
    app.rbのドキュメントを作成してください
    ```
- [ ] 最後に、GitLab Duo Chat :robot: に何でも聞いてください。この課題にコメントを追加し、フレンドリーなインストラクターにメンションして、最良の質問/回答を共有してください。

## :beetle: 脆弱性の分析

### 1. アプリケーションのセキュリティポジションを理解する

- [ ] 最新のパイプラインが完了していることを確認する
  1. 左のサイドバーで、`Build > Pipelines`を選択します。
  2. 提供される情報をすべて確認し、ジョブの結果と情報タブを探索してください。
- [ ] プロジェクトの脆弱性の概要を確認する
  1. 左のサイドバーで、`Secure > Vulnerability report`を選択して完全なレポートを表示します。
  2. いくつかの脆弱性があることに注意してください。トークンの漏洩やコンテナの問題など、GitLabがポリシーとプラットフォームの力を使って迅速に修正をサポートします。
- [ ] **可能なSQLインジェクション**の脆弱性を探す
  1. 次のようにレポートをフィルタリングします：
    - `Status=All statuses`
    - `Severity=Low`
    - `Tool=SAST`
  2. 脆弱性を選択します。
- [ ] 脆弱性についての詳細情報、悪用方法、修正方法を確認する
  1. 脆弱性のページの一番下にある`Try it out`ボタンをクリックします。
  2. 右側にポップアップが表示され、[GitLabの脆弱性の説明](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#explaining-a-vulnerability)機能を使用して、SQLインジェクションのリスクとアプリケーションの脆弱性についての説明が表示されます。
  3. **_Fixed Code_**セクションを確認し、`id`の周りに`sanitize_sql(id)`を追加すると、ほとんどの攻撃から保護されることがわかります。この知識は、後でワークショップで使用します。

> もし、なぜこの応答が発生したのか気になる場合は、**_Show prompt_** をクリックして、GitLabに送信された完全なプロンプトを表示して、提案された修正を生成するための情報を確認してください。

### 2\. 脆弱性の文脈を理解する

- [ ] コードの変更を行う前に、特定の関数についての追加の文脈を知りたい場合はどうすればよいでしょうか？
  1. **_Location_** セクションのリンクされたファイルをクリックして、`db.rb` ファイルに移動します。
  2. `db.rb` ファイルに移動したら、42行目のSQLインジェクションの脆弱性がある行を見つけ、`get_specific` 関数全体をハイライトします。
  3. すると、コードの左側に小さな疑問符が表示されるので、それをクリックします。
  4. 右側には、ハイライトされたコードの説明が自然言語で表示される「Code Explanation」という小さなポップアップが表示されます。
  5. 他のコードセクションもハイライトしてみてください。
  6. この時点で、なぜSQLインジェクションの脆弱性が発生しているのか、どのように発生しているのかを完全に把握することができるはずです。

## :art: 修正を実装する

### 1\. AIによる解決
- [ ] セキュリティの脆弱性を修正するために、Code Suggestionsを使用する前に、_Resolve with AI_ 機能も使用します：

1. 左のサイドバーで、`Secure > Vulnerability report` を選択して、再度レポート全体を表示します。

2. 以下のようにレポートをフィルタリングします：
   - `Status=All statuses`
   - `Severity=Critical`
   - `Tool=SAST`。
3. 表示される脆弱性のいずれかをクリックします。脆弱性のビューに移動したら、**Resolve with AI** をクリックします。
4. これにより、脆弱性を修正する新しいマージリクエストが開かれます。**Changes** タブをクリックして、導入される修正を表示します。
5. 後でこの修正が脆弱性を修正していることを確認するために戻ってくることができますが、その間は次のセクションに進みます。

### 2\. SQLインジェクションの修正
- [ ] SQLインジェクションの脆弱性についてのより詳細な文脈を把握したので、GitLab Code Suggestionsを使用して修正を試みましょう：

1. コードの変更を行う前に、作業を追跡するためにマージリクエストを作成する必要があります。
2. 既にこの作業を追跡するためのイシューがあるので、左側のナビゲーションを使用して **Plan \> Issues** に移動します。
3. 作成したイシューに移動し、**Create merge request** をクリックします。
4. 表示されるページで **Mark as draft** のチェックを外し、他の設定はそのままにして、一番下までスクロールして **Create merge request** をクリックします。
5. 次に、右上の **Code** をクリックし、**Open in Web IDE** をクリックします。
6. IDEに入ったら、**lib/tanukiracing/db.rb** を経由して、42行目の関数を確認します。Explain This Vulnerabilityを使用して学んだことを活用して、コードの行を次のように変更します：

```plaintext
Leaderboard.first.where("player = ?", sanitize_sql(id))
```

> このSQLの脆弱性によって提案される変更は、参照するSQLの脆弱性によって異なる可能性があることに注意してください。

### 3\. 新しいメソッドを追加する

- [ ] db.rbファイルには、リーダーを削除するメソッドが存在しないことに注意してください。これを修正しましょう：

1. プロジェクトマネージャーは、偽のレコードを削除するためにこれができるだけ早く必要であると明確に指示しています。したがって、**get_specific**の後の新しい行に、以下のプロンプトを使用してこの関数を生成しましょう：

```plaintext
# active recordを使用してリーダーボードを削除する関数を書く
```

2. 次に、書いた新しいメソッドを使用するために、**_lib/tanukiracing/app.rb_**ファイルに移動します。クラス内の新しい行に、以下のプロンプトを使用して新しい削除ルートを生成します：

```plaintext
# TanukiRacing::DBからdelete_leader関数を呼び出すポストルートを書く
```

> コードの提案は、関数の書き方を参照するために現在のファイルを使用するため、最終結果のためにわずかな編集が必要な場合があります。

### 4\. 新しいクラスを作成する

- [ ] プロジェクトマネージャーは、プレイヤーのトラックタイムを合計するために使用される新しい計算機クラスを作成するように依頼しています：

1. まず、**lib/tanukiracing**フォルダを右クリックし、**新しいファイル**を右クリックします。この新しいファイルの名前を**_calc.rb_**とします。
2. 次に、以下のプロンプトを追加して、Code Suggestionsに私たちが書こうとしている内容を伝えます：

```plaintext
#他の関数が使用するための計算機クラスを定義する
```

3. Enterキーを押して、しばらくすると提案が表示されます。提案が表示されると、TABキーを押してそれらを受け入れます。もし詰まった場合は、スペースバーまたはEnterキーを押してみてください。
4. Code Suggestionsは非常に詳細な計算機関数を書き、最終的にはループしますが、5つのメソッド後に停止しても構いません。
5. Code SuggestionsはRubyファイルだけでなく、複数の言語をサポートしており、プロジェクトごとに複数の言語を使用できます。最新のプロジェクトのリストにアクセスするために**ai-sandbox/**フォルダに移動します。
6. プロジェクトのいずれかを選択し、Code Suggestionsを使用してハローワールドの例やより高度な例を書くことを試してみてください。インストラクターがこれを行う時間を与えますが、48時間以内に必要なテストを行うためにインフラにアクセスできることも忘れないでください。
7. 次に、このコードをメインにコミットします。左側の**ソースコントロール**ボタンをクリックし、コミットメッセージを書きます。次に、**コミット＆プッシュ**をクリックします。
8. 次に、表示されるドロップダウンメニューで、mrブランチにコミットするように確認し、ポップアップウィンドウで**マージリクエストに移動**ボタンをクリックします。

## :microscope: コードのレビュー

### 3\. MR内のAI

- [ ] MRに戻ったら、コードの変更がパイプラインを再実行したことがわかるはずです。

1. 多くの変更を行ったので、AIの**概要ノートを表示**ボタンを使用して、行ったすべての変更について詳細なコメントを追加しましょう。
2. これを行うには、マージリクエストビューの右上にある_Code_の隣にある3つのドットボタンを見つけてクリックし、表示されるドロップダウンで**概要ノートを表示**オプションをクリックします。
3. 実行されたパイプラインが完了するのを待つ必要があるかもしれませんが、完了したら、マージリクエストの文脈で行ったすべての変更の簡単なまとめが表示されます。この機能は、課題の内容にも存在します。このプロジェクトの_optional_な課題内でそれを試すための手順を見つけることができます。
4. コードを追加し、セキュリティの結果を確認したので、セキュリティを保つためにいくつかのテストも追加したいです。
5. MRビューの上部にある変更タブに移動し、db.rbファイルで行った変更を見つけます。
6. 次に、そのファイルのビューの右上隅にある3つのドットをクリックします。
7. その後、**テストケースの提案**をクリックすると、右側にポップアップが表示され、ユニットテストのためにプロジェクトに追加できるいくつかのテストケースの提案が表示されます。


## :rocket: 結論

1. これで、このラボの実践的な部分は終了ですが、[issues](https://gitlab.com/gitlab-learn-labs/sample-projects/ai-workshop/workshop-project/-/issues)をチェックすると、VSCでコードの提案を有効にするための追加の手順や、PlanステージのAI/ML機能のいくつかを紹介する手順があります。
