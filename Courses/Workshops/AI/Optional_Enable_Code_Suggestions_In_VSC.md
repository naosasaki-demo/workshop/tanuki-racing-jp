あなたは他のサポートされている[Web IDEに関するブログ](https://about.gitlab.com/blog/2023/06/01/extending-code-suggestions/)も見つけることができます。

# ステップ1: VSCの接続
1. デバイスにVSCがインストールされていることを確認してください。インストールされていない場合は、[ここから](https://code.visualstudio.com/Download)ダウンロードできます。

2. 次に、VSCのインスタンスに移動し、左側のナビゲーションメニューで**Extensions**ボタンをクリックします。そこで、検索バーに「GitLab Workflow」と入力してインストールします。

3. インストールと有効化が完了したら、設定アイコンをクリックし、**Extension settings**をクリックします。設定の上部にある**Enable code completion**の横にあるチェックボックスをクリックします。

# ステップ2: GitLabでの認証
  **OAuthメソッド（推奨）**
1. まだVSC上にいる場合、GitLabアカウントを追加します。以前にこれを行ったことがある場合でも、設定が変更されている可能性があるため、アクセスをリセットしてください：
    - macOSでは、Shift + Command + Pを押します。
    - Windowsでは、Shift + Control + Pを押します。

2. 次に、**_'GitLab: Authenticate with GitLab.com'_**と入力し、同じ名前のクイックアクションをクリックし、警告リンクをたどってgitlab.comの認証ポータルを開きます。

3. 認証が完了すると、VSCに戻るためのアラートが表示されます。この時点で準備が整い、パーソナルアクセストークンの方法を試す必要はありません。

  **パーソナルアクセストークンの方法**
1. gitlab.comの表示に移動し、右上のアカウントアイコンをクリックします。表示されるドロップダウンメニューで**Preferences**をクリックします。

2. 次に、左側のナビゲーションメニューで**Access Tokens**をクリックします。**_Personal Access Tokens_**ページに移動したら、トークンにVSCという名前を付け、有効期限を設定します（少なくとも30日を推奨）、そしてすべてのスコープを選択します。

3. 設定がすべて完了したら、**Create personal access token**をクリックし、トークンをローカルにコピーします。後でワークショップで使用する必要があります。

4. VSCにGitLabアカウントを追加します。以前にこれを行ったことがある場合でも、設定が変更されている可能性があるため、トークンをリセットしてください：
    - macOSでは、Shift + Command + Pを押します。
    - Windowsでは、Shift + Control + Pを押します。

5. 次に、**'GitLab: Add Account to VS Code'** と入力し、同じ名前のクイックアクションをクリックします。

6. 次に、'https://gitlab.com/' を入力し、先ほど取得した個人アクセストークンを提供します。

これで、VSCでコードの提案が表示されるようになります。Web IDEと同じように、JetBrains IDE、Neovimなどの他のエディタを使用したい場合は、[こちらのドキュメント](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/#supported-editor-extensions)をご覧ください。

# ステップ 3: ワークショッププロジェクトのクローン
1. 今度は、VSC上でプロジェクトを準備する必要があります。**_workshop-project_** のホームページに移動し、**clone** ボタンをクリックします。次に、**_Clone with HTTPS_** オプションのURL全体をコピーします。
