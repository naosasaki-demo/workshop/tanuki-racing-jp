### セキュリティスキャンをパイプラインに追加するためのマージリクエストを作成する

### テーマ

このセクションでは、セキュリティの実践としての左シフトに焦点を当て、コードの変更がコミット後にセキュリティ結果を表示するようになることを説明します。

# ステップ1: セキュリティスキャンの追加

1. まず、フォークしたプロジェクトのメインページにいることを確認してください。この手順を別のタブ/画面で開いたまま、タスクを完了してください。

2. 準備ができたら、左側のナビゲーションメニューを使用して**ビルド \> パイプラインエディタ**に移動します。ここでは、メインブランチのパイプラインの現在の設定が表示されます。以下で詳細に説明しますが、2つのステージがあります。

3. このパイプラインはセキュリティスキャンをほとんど行っておらず、現在は単純なユニットテストのみが定義されています。変更を追加するために新しいブランチを作成しましょう。左側のナビゲーションメニューを使用して**コード \> ブランチ**に移動し、**新しいブランチ**をクリックします。ブランチの名前を **_secure-pipeline_** とし、 **_main_** を基にしていることを確認し、**ブランチを作成**をクリックします。

4. 再び左側のナビゲーションメニューを使用して**ビルド \> パイプラインエディタ**に移動し、エディタページに戻ります。エディタビューの左上にあるブランチのドロップダウンが **_secure-pipeline_** を表示しているか、表示されていない場合はブランチを選択します。次に、以下のコードにパイプラインのyamlを変更します。

```plaintext
image: docker:latest

services:
  - docker:dind

variables:
  CS_DEFAULT_BRANCH_IMAGE: $CI_REGISTRY_IMAGE/$CI_DEFAULT_BRANCH:$CI_COMMIT_SHA
  DOCKER_DRIVER: overlay2
  ROLLOUT_RESOURCE_TYPE: deployment
  DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
  RUNNER_GENERATE_ARTIFACTS_METADATA: "true"
  DAST_BAS_DISABLED: "true"
  CI_DEBUG_TRACE: "true"
  # CI_PROJECT_PATH_SLUG: "tanukiracing"
  SECRET_DETECTION_EXCLUDED_PATHS: "Courses"
  

stages:
  - build
  - test

include:
  - template: Jobs/Test.gitlab-ci.yml
  - template: Jobs/Container-Scanning.gitlab-ci.yml
  - template: Code-Quality.gitlab-ci.yml
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml
  - template: Jobs/SAST-IaC.gitlab-ci.yml

build:
  stage: build
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA
  script:
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE .
    - docker push $IMAGE

sast:
  variables:
      SEARCH_MAX_DEPTH: 12
```

5.  **_include_** セクションを見ると、いくつかのセキュリティテンプレートがプロジェクトに取り込まれていることがわかります。これらは、 **_stages_** に基づいて実行されるさまざまなスキャンとジョブを定義しています。テンプレートの詳細を確認するには、**完全な構成**をクリックすると、すべてのテンプレートが取り込まれた実際のパイプラインのyamlが表示されます。また、左上のブランチアイコンをクリックして、特定のテンプレートに移動し、その定義を確認することもできます。

6. 再び**編集**タブをクリックして通常のエディタに戻ります。ファイルの末尾には、 **_Jobs/SAST.gitlab-ci.yml_** テンプレートを介して取り込まれたSASTジョブの値をオーバーライドするための追加のジョブが定義されていることに注意してください。

7. これで変更が反映されましたので、ページの下部にある**変更をコミットする**をクリックしましょう。

> 問題が発生した場合は、左側のナビゲーションメニューを使用して**CI/CD -\> パイプライン**に進み、**パイプラインを実行**をクリックし、 **_security-workshop-pipeline_** を選択して再度**パイプラインを実行**します。

> [GitLab CI/CDのドキュメント](https://docs.gitlab.com/ee/ci/)

# ステップ2: GitLab Duoに質問する

1. 実行中のパイプラインの場所がわからなかった場合や、パイプラインの中の`includes`キーワードについて詳しく知りたい場合はどうすればよいでしょうか？新しいGitLab Duo Chatbotを使用してそれらの情報を取得することができます。

2. 画面の左下にある **? ヘルプ** ボタンをクリックし、**GitLab Duo Chat** をクリックします。これにより、画面の右側にチャットのプロンプトが表示され、質問を入力することができます。"GitLabにはどのようなセキュリティ機能がありますか？"と入力してみてください。

3. 今後のワークショップでは、このチャットボットを使用して質問があれば何でも聞くことができます。もし行き詰まった場合は、コーディングに関する質問もできます。

# ステップ3: マージリクエストの作成

1. 新しいパイプラインを実際にマージするために、左側のナビゲーションメニューを使用して**Code \> Branches**に進み、**secure-pipeline**セクションの**マージリクエスト**をクリックします。**これを行う前に、フォーク関係を解除していることを確認してください**

2. 表示されたページで、 **_マージオプション_** までスクロールし、**マージリクエストが承認されたときにソースブランチを削除する**のチェックを外します。他の設定はそのままにして、**マージリクエストを作成**をクリックします。

3. マージコンフリクトがあれば解決してくださいが、パイプラインが開始されるはずです。ハイパーリンクをクリックすると、パイプラインビューに移動し、yamlファイルに追加したさまざまなジョブを確認することができます。この間に、コンプライアンスフレームワークパイプラインの設定を進める予定ですが、しばらくしてスキャナーの結果を確認します。
